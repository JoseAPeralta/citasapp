import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/*
    Angular Fire2
*/
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
/*
    Angular Fire AuthUi
*/
import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';
/* 
    Angular Material
*/
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { ModalComponent } from './components/shared/modal/modal.component';
/* 
    My components
*/
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { NewAppointmentComponent } from './components/appointment/new-appointment/new-appointment.component';
import { ShowAppointmentsComponent } from './components/appointment/show-appointments/show-appointments.component';
import { ShowAppointmentComponent } from './components/appointment/show-appointment/show-appointment.component';
import { PricesComponent } from './components/prices/prices.component';
import { NewInvoiceComponent } from './components/invoice/new-invoice/new-invoice.component';
import { ShowInvoicesComponent } from './components/invoice/show-invoices/show-invoices.component';
import { DetailInvoicesComponent } from './components/invoice/detail-invoices/detail-invoices.component';
import { LoginComponent } from './components/login/login.component';
/* 
    Services
*/
import { AuthService } from './services/auth.service';
import { AppointmentService } from './services/appointment.service';

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    {
      scopes: [
        'public_profile',
        'email',
        'user_likes',
        'user_friends'
      ],
      customParameters: {
        'auth_type': 'reauthenticate',
        prompt: 'select_account'
      },
      provider:firebase.auth.EmailAuthProvider.PROVIDER_ID,
    },
  ],
  tosUrl: '/terminos',
  signInSuccessUrl: '/nuevacita',
  privacyPolicyUrl: '/privacidad',
  credentialHelper: firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NewAppointmentComponent,
    ShowAppointmentsComponent,
    ShowAppointmentComponent,
    PricesComponent,
    NewInvoiceComponent,
    ShowInvoicesComponent,
    DetailInvoicesComponent,
    LoginComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    MaterialModule,
    BrowserAnimationsModule,
    MatIconModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig)
  ],
  providers: [
    AuthService,
    AppointmentService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ModalComponent, ]
})
export class AppModule { }
