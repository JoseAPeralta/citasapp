import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppointmentInterface } from '../models/appointment';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  private appointmentsCollection: AngularFirestoreCollection<AppointmentInterface>;
  private appointmentsDocument: AngularFirestoreDocument<AppointmentInterface>;
  private appointments: Observable<AppointmentInterface[]>;
  private appointment: Observable<AppointmentInterface>;
  private uid: string;
  date = new Date();
  today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  
  
  constructor(private afs: AngularFirestore, private auth: AuthService) {
    this.appointmentsCollection = afs.collection<AppointmentInterface>('appointments');
    
   }

  addAppointment(appointment: AppointmentInterface){
    this.appointmentsCollection.add(appointment);
  }

  getAppointments(filterDate = this.today){
    this.uid = this.auth.getUID()
    this.appointmentsCollection = this.afs.collection('appointments', ref => ref.where('uid', '==', this.uid ).where('date', '==', filterDate ));
    this.appointments = this.appointmentsCollection.snapshotChanges()
      .pipe(map(actions => actions.map(a => {
        const data = a.payload.doc.data() as AppointmentInterface;
        data.id = a.payload.doc.id;
        return data;
      }))
      );
    return this.appointments;
  }
  
  getAppointment(id: string){
    this.appointmentsDocument = this.afs.doc<AppointmentInterface>(`appointments/${id}`);
    return this.appointment = this.appointmentsDocument.snapshotChanges().pipe(map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as AppointmentInterface;
        data.id = action.payload.id;
        return data;
      }
    }));
  }

  updateAppointment(appointment:AppointmentInterface){
    this.appointmentsDocument = this.afs.doc<AppointmentInterface>(`appointments/${appointment.id}`);
    this.appointmentsDocument.update(appointment);
  }

  deleteAppointment(id:string){
    this.appointmentsDocument = this.afs.doc<AppointmentInterface>(`appointments/${id}`);
    this.appointmentsDocument.delete();
  }
  
  
}
