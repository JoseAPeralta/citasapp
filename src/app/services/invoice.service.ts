import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { InvoiceInterface } from '../models/invoice';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  
  private invoicesCollection: AngularFirestoreCollection<InvoiceInterface>;
  private invoicesDocument: AngularFirestoreDocument<InvoiceInterface>;
  private invoices: Observable<InvoiceInterface[]>;
  private invoice: Observable<InvoiceInterface>;
  private uid: string;

  constructor(private afs: AngularFirestore, private auth: AuthService) {
    this.invoicesCollection = afs.collection<InvoiceInterface>('invoices');
   }

  addInvoice(invoice: InvoiceInterface){
    this.invoicesCollection.add(invoice);
  }

  getInvoices(clientId){
    this.uid = this.auth.getUID()
    this.invoicesCollection = this.afs.collection('invoices', ref => ref.where('uid', '==', this.uid ).where('clientId', '==', clientId ));
    this.invoices = this.invoicesCollection.snapshotChanges()
      .pipe(map(actions => actions.map(a => {
        const data = a.payload.doc.data() as InvoiceInterface;
        data.id = a.payload.doc.id;
        return data;
      }))
      );
    return this.invoices;
  }

  getInvoice(id: string){
    this.invoicesDocument = this.afs.doc<InvoiceInterface>(`invoices/${id}`);
    return this.invoice = this.invoicesDocument.snapshotChanges().pipe(map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as InvoiceInterface;
        data.id = action.payload.id;
        return data;
      }
    }));
  }

  updateInvoice(invoice:InvoiceInterface){
    this.invoicesDocument = this.afs.doc<InvoiceInterface>(`invoices/${invoice.id}`);
    this.invoicesDocument.update(invoice);
  }
    
}
