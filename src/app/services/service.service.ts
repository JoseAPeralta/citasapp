import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ServiceInterface } from '../models/service';
import { AuthService } from '../services/auth.service';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private servicesCollection: AngularFirestoreCollection<ServiceInterface>;
  private servicesDocument: AngularFirestoreDocument<ServiceInterface>;
  private services: Observable<ServiceInterface[]>;
  private service: Observable<ServiceInterface>;
  private uid: string;

  constructor(private afs: AngularFirestore, private auth: AuthService) {
    this.servicesCollection = afs.collection<ServiceInterface>('services');
   }

  addService(service: ServiceInterface){
    this.servicesCollection.add(service);
  }

  getServices(){
    this.uid = this.auth.getUID()
    this.servicesCollection = this.afs.collection('services', ref => ref.where('uid', '==', this.uid ));
    this.services = this.servicesCollection.snapshotChanges()
      .pipe(map(actions => actions.map(a => {
        const data = a.payload.doc.data() as ServiceInterface;
        data.id = a.payload.doc.id;
        return data;
      }))
      );
    return this.services;
  }

  deleteService(id:string){
    this.servicesDocument = this.afs.doc<ServiceInterface>(`services/${id}`);
    this.servicesDocument.delete();
  }
  
}
