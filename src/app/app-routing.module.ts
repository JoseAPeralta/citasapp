import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

import { NewAppointmentComponent } from './components/appointment/new-appointment/new-appointment.component';
import { ShowAppointmentsComponent } from './components/appointment/show-appointments/show-appointments.component';
import { ShowAppointmentComponent } from './components/appointment/show-appointment/show-appointment.component';
import { PricesComponent } from './components/prices/prices.component';
import { NewInvoiceComponent } from './components/invoice/new-invoice/new-invoice.component';
import { ShowInvoicesComponent } from './components/invoice/show-invoices/show-invoices.component';
import { DetailInvoicesComponent } from './components/invoice/detail-invoices/detail-invoices.component';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'login', component: LoginComponent },
      { path: 'nuevacita', component: NewAppointmentComponent },
      { path: 'citas', component: ShowAppointmentsComponent },
      { path: 'cita/:citaId', component: ShowAppointmentComponent },
      { path: 'servicios', component: PricesComponent },
      { path: 'generarfactura', component: NewInvoiceComponent },
      { path: 'facturas', component: ShowInvoicesComponent },
      { path: 'factura/:facturaId', component: DetailInvoicesComponent },
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
