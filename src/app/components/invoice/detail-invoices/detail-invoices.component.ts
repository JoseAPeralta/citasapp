import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { InvoiceService } from '../../../services/invoice.service';
import { InvoiceInterface } from '../../../models/invoice';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { ModalComponent } from '../../shared/modal/modal.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-invoices',
  templateUrl: './detail-invoices.component.html',
  styleUrls: ['./detail-invoices.component.scss']
})
export class DetailInvoicesComponent implements OnInit {
  date = new Date();
  today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  invoiceForm: FormGroup;
  paymentTotal= 0;
  currentInvoice: InvoiceInterface = {
    id: '',
    uid: '',
    clientId: '',
    date: '',
    amountOther: 0,
    amountTotal: 0,
    amountTax: 0,
    amountGross: 0,
    payments: [{
      
    }],
    services: [{
      
    }]
  }
  constructor(private appointmentService: InvoiceService, private Auth: AuthService, public dialog: MatDialog, private route: ActivatedRoute) {
    this.currentInvoice.id = this.route.snapshot.params['facturaId'];
    this.getInvoice(this.currentInvoice.id);
    // this.invoiceForm = new FormGroup({
    //     'id' : new FormControl ('', Validators.required),
    //     'uid': new FormControl ('' , Validators.required),
    //     'clientId': new FormControl ('' , [Validators.required, Validators.pattern("[1-9]{7,}")]),
    //     'date': new FormControl (this.today , Validators.required),  
    //     'hour': new FormControl ('' , Validators.required),
    //     'estimatedDuration': new FormControl (2 , [Validators.required, Validators.min(1)]),
        
    //     'note': new FormControl (''),
    // })
   }

  ngOnInit() {
  }

  getInvoice(id:string){
    this.appointmentService.getInvoice(id).subscribe(invoice => {
    this.currentInvoice = invoice;
    console.log(this.currentInvoice);
    this.currentInvoice.date = new Date (this.currentInvoice.date.seconds * 1000)
    // this.currentInvoice.date = new Date (this.currentInvoice.date.seconds * 1000)
    // this.invoiceForm.setValue(this.currentInvoice);
    });
  }

  updateInvoice(){
    let currentAppointment: InvoiceInterface = this.invoiceForm.value;
    if (this.invoiceForm.valid) {
      try {
        this.appointmentService.updateInvoice(currentAppointment);
        this.dialog.open(ModalComponent, {data: { title: `Pago realizado con exito`, button2Text: 'aceptar'}});
      } catch (error) {
        this.dialog.open(ModalComponent, {data: { title: 'No se pudo realizar el pago', button2Text: 'aceptar'}});
      }
    }
  }

}
