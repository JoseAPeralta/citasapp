import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { InvoiceInterface } from '../../../models/invoice';
import { InvoiceService } from '../../../services/invoice.service';
import { ModalComponent } from '../../shared/modal/modal.component';
@Component({
  selector: 'app-show-invoices',
  templateUrl: './show-invoices.component.html',
  styleUrls: ['./show-invoices.component.scss']
})
export class ShowInvoicesComponent implements OnInit {
  existData: boolean;
  invoices: any;
  invoiceId: any;
  invoiceDate: any;
  invoicePayment: Array<any> = [];

  constructor(private invoiceService:InvoiceService, public dialog: MatDialog) {
    this.existData = false;
   }

  ngOnInit() {
  }

  getInvoices(){
    this.invoiceService.getInvoices(this.invoiceId).subscribe(invoices=>{
      this.invoices = invoices;
      //console.log(invoices)
      invoices.length > 0 ? this.existData = true : this.existData = false;
    })
    console.log(this.invoices)
  }

  calculatePayment(invoice: any){
    this.invoicePayment = [{}]
    if (invoice) {
      for (const i of invoice) {
        console.log("Soy el valor de la factura " + i + invoice.payments)
        this.invoicePayment[i] = invoice.payments.amount
        console.log(this.invoicePayment[i])
      }
    }
    
    // this.invoicePayment = this.invoices.reduce((sum, value) => (typeof value.payments == "number" ? 
    //                   sum + value.payments : sum), 0);
    // console.log(this.invoicePayment)
  }
}
