import { Component, OnInit } from '@angular/core';
import { InvoiceInterface } from '../../../models/invoice';
import { InvoiceService } from '../../../services/invoice.service';
import { ServiceService } from '../../../services/service.service';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../shared/modal/modal.component';
@Component({
  selector: 'app-new-invoice',
  templateUrl: './new-invoice.component.html',
  styleUrls: ['./new-invoice.component.scss']
})
export class NewInvoiceComponent implements OnInit {

  date = new Date();
  today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  maxDate = new Date(this.date.getFullYear() + 1, this.date.getMonth(), this.date.getDate());
  invoices: any;
  invoiceForm: FormGroup;
  services: any;
  payments: any;
  amountPayment: any;
  existData: boolean;
  selectedInvoice: Array<any> = [];
  newInvoice: InvoiceInterface = {
    uid: '',
    clientId: '',
    date: this.today,
    amountGross: 0,
    amountTax: 0,
    amountOther: 0,
    amountTotal: 0,
    payments:{
      date: this.today,
      amount: ''
    },
    services: {
        id:'',
        service:'',
        price: 0,
        profit: 0,
        tax: 0
      },
    
  }

  constructor(private invoiceService: InvoiceService, private serviceService: ServiceService, private fb: FormBuilder, private Auth: AuthService, public dialog: MatDialog) {
    this.Auth.getAuth().subscribe( auth => {this.newInvoice.uid = auth.uid;});
    this.invoiceForm = this.fb.group({
      uid: [this.newInvoice.uid , Validators.required],
      clientId: [this.newInvoice.clientId, [Validators.required, Validators.pattern("([1-9]{1,})*-([0-9]{3,4})*-([0-9]{4,})")]],
      date: [this.newInvoice.date, Validators.required],
      amountGross: [this.newInvoice.amountGross],
      amountTax: [this.newInvoice.amountTax],
      amountOther: [this.newInvoice.amountOther],
      amountTotal: [this.newInvoice.amountTotal, Validators.required],
      payments: [this.payments, Validators.required],
      services: [this.selectedInvoice],
    })
    this.getService();
    console.log(this.newInvoice.date)
   }

  ngOnInit() {
    
  }
  
  calculateAmountTax(){
    this.newInvoice.amountTax = 0
    this.newInvoice.amountTax = this.selectedInvoice.reduce((sum, value) => (typeof value.price == "number" ? 
                      sum + (value.price * (value.tax/100)) : sum), 0);
  }

  calculateAmountGross(){
    this.newInvoice.amountGross = 0;
    this.newInvoice.amountGross = this.selectedInvoice.reduce((sum, value) => (typeof value.price == "number" ? 
                        sum + (value.price + (value.price * (value.profit/100))) : sum), 0);
    this.newInvoice.amountGross = this.newInvoice.amountGross + this.invoiceForm.value.amountOther;
  }

  calculateAmountTotal(){
    this.newInvoice.amountTotal = this.newInvoice.amountGross + this.newInvoice.amountTax;
  }

  calculateAmounts(){
    this.calculateAmountTax();
    this.calculateAmountGross();
    this.calculateAmountTotal();
  }

  getChecked(id:any, service:any, tax:any, profit:any, price:any){
    if (!this.selectedInvoice.find(e => e.id === id)) {
      this.selectedInvoice.push({id, service, tax, profit, price});
    }else{
      this.selectedInvoice = this.selectedInvoice.filter((i) => i.id !== id);
    }
    this.calculateAmounts();
  }

  resetForm(){
    this.newInvoice=({
      uid: this.newInvoice.uid,
      id: '',
      clientId: '',
      date: this.today,
      amountOther: 0,
      amountTotal: 0,
      payments: 0,
    })
    this.invoiceForm.reset(this.newInvoice)
  }

  addInvoice(){
    this.getPayment();
    this.invoiceForm.patchValue({uid: this.newInvoice.uid, 
                                amountGross: this.newInvoice.amountGross, 
                                amountTax: this.newInvoice.amountTax,
                                amountTotal: this.newInvoice.amountTotal,
                                payments: this.payments
                              });
    if (this.invoiceForm.valid) {
      try {
        console.log(this.invoiceForm.value)
        this.invoiceService.addInvoice(this.invoiceForm.value);
        this.dialog.open(ModalComponent, {data: { title: `Factura creada con éxito`, button2Text: 'aceptar'}});
      } catch (error) {
        this.dialog.open(ModalComponent, {data: { title: 'Ocurrio un error', button2Text: 'aceptar'}});
      }
    }else{
      this.dialog.open(ModalComponent, {data: { title: 'Debe rellenar todos los campos', button2Text: 'aceptar'}});
    }
  }
  
  getPayment(){
    this.payments = [{
      date: this.today,
      amount: this.invoiceForm.value.payments
    }]
  }

  getService(){
    this.serviceService.getServices().subscribe(services=>{
      this.services = services;
      services.length > 0 ? this.existData = true : this.existData = false;
    })
  }
}
