import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  email:string;
  isLogin:boolean;
  links = []
  constructor(private Auth: AuthService) {
    this.links = [
      {
        text: 'Nueva cita',
        url: 'nuevacita',
        icon: 'create'
      },
      {
        text: 'Ver citas',
        url: 'citas',
        icon: 'today'
      },
      {
        text: 'Generar factura',
        url: 'generarfactura',
        icon: 'note_add'
      },
      {
        text: 'facturas',
        url: 'facturas',
        icon: 'description'
      },
      {
        text: 'Servicios',
        url: 'servicios',
        icon: 'list_alt'
      },
      {
        text: 'Ver estadisticas',
        url: '#',
        icon: 'equalizer'
      }
    ]
  }
  ngOnInit() {
    this.Auth.getAuth().subscribe( auth => {
      if (auth) {
        this.isLogin = true;
        this.email = auth.email;
      } else {
        this.isLogin = false;
      }
    });
  }
  logouts(){
    this.Auth.logout();
  }
}
