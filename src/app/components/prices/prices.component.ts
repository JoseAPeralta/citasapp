import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ServiceInterface } from '../../models/service';
import { AuthService } from '../../services/auth.service';
import { ServiceService } from '../../services/service.service';
import { ModalComponent } from '../shared/modal/modal.component';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss']
})
export class PricesComponent implements OnInit {
  existData: boolean;
  services: any;
  serviceForm: FormGroup;
  newService: ServiceInterface = {
    uid: '',
    service: '',
    price: 0,
    profit: 0,
    tax: 0,
  }
  constructor(private serviceService: ServiceService, private Auth: AuthService, public dialog: MatDialog) {
    this.existData = false;
    this.Auth.getAuth().subscribe( auth => {this.newService.uid = auth.uid;});
    this.serviceForm = new FormGroup({
        'uid': new FormControl ('' , Validators.required),
        'service': new FormControl ('' , [Validators.required, Validators.min(4)]),
        'price': new FormControl (1 , [Validators.required, Validators.min(1)]),
        'profit': new FormControl (0 , [Validators.required, Validators.max(100)]),
        'tax': new FormControl (0 , [Validators.required, Validators.max(100)]),
    })
    this.serviceForm.setValue(this.newService);
    this.getService();
   }

  ngOnInit() {
  }
  resetForm(){
    this.newService=({
      id: '',
      uid: this.newService.uid,
      service: '',
      price: 0,
      profit: 0,
      tax: 0,
    })
    this.serviceForm.reset(this.newService)
  }

  addService(){
    this.serviceForm.patchValue({uid: this.newService.uid})
    console.log(this.serviceForm.value)
    if (this.serviceForm.valid) {
      try {
        this.serviceService.addService(this.serviceForm.value);
        this.dialog.open(ModalComponent, {data: { title: `Servicio añadido con éxito`, button2Text: 'aceptar'}});
        this.resetForm();
      } catch (error) {
        console.log(error)
        this.dialog.open(ModalComponent, {data: { title: 'Ocurrio un error', button2Text: 'aceptar'}});
      }
    }else{
      this.dialog.open(ModalComponent, {data: { title: 'Debe rellenar todos los campos', button2Text: 'aceptar'}});
    }
  }
  
  getService(){
    this.serviceService.getServices().subscribe(services=>{
      this.services = services;
      services.length > 0 ? this.existData = true : this.existData = false;
    })
    console.log(this.services)
  }

  deleteService(id:string){
    const confirmacion = confirm('¿Seguro que quieres eliminar la cita?');
    if (confirmacion) {
      this.serviceService.deleteService(id);
    }
  }
}
