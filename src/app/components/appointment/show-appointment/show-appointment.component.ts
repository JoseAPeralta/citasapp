import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { AppointmentService } from '../../../services/appointment.service';
import { AppointmentInterface } from '../../../models/appointment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../shared/modal/modal.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-appointment',
  templateUrl: './show-appointment.component.html',
  styleUrls: ['./show-appointment.component.scss']
})
export class ShowAppointmentComponent implements OnInit {
  date = new Date();
  today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  maxDate = new Date(this.date.getFullYear() + 1, this.date.getMonth(), this.date.getDate());
  appointmentForm: FormGroup;
  currentAppointment: AppointmentInterface = {
    id: '',
    uid: '',
    date: '',
    hour: '',
    estimatedDuration: 0,
    clientId: '',
    note: '',
  }
  constructor(private appointmentService: AppointmentService, private Auth: AuthService, public dialog: MatDialog, private route: ActivatedRoute) { 
    this.currentAppointment.id = this.route.snapshot.params['citaId'];
    this.getAppointment(this.currentAppointment.id);
    this.appointmentForm = new FormGroup({
        'id' : new FormControl ('', Validators.required),
        'uid': new FormControl ('' , Validators.required),
        'date': new FormControl ('' , Validators.required),  
        'hour': new FormControl ('' , Validators.required),
        'estimatedDuration': new FormControl (2 , [Validators.required, Validators.min(1)]),
        'clientId': new FormControl ('' , [Validators.required, Validators.pattern("([1-9]{1,})*-([0-9]{3,4})*-([0-9]{4,})")]),
        'note': new FormControl (''),
    })
  }

  ngOnInit() {
  }

  getAppointment(id:string){
    this.appointmentService.getAppointment(id).subscribe(appointment => {
    this.currentAppointment = appointment;
    this.currentAppointment.date = new Date (this.currentAppointment.date.seconds * 1000)
    this.appointmentForm.setValue(this.currentAppointment);
    });
  }

  updateAppointment(){
    let currentAppointment: AppointmentInterface = this.appointmentForm.value;
    if (this.appointmentForm.valid) {
      try {
        this.appointmentService.updateAppointment(currentAppointment);
        this.dialog.open(ModalComponent, {data: { title: `Cita modificada con éxito`, button2Text: 'aceptar'}});
      } catch (error) {
        this.dialog.open(ModalComponent, {data: { title: 'No se pudo modificar la cita', button2Text: 'aceptar'}});
      }
    }
  }
}
