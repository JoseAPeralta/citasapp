import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppointmentService } from '../../../services/appointment.service';



@Component({
  selector: 'app-show-appointments',
  templateUrl: './show-appointments.component.html',
  styleUrls: ['./show-appointments.component.scss']
})
export class ShowAppointmentsComponent implements OnInit {
  public date = new Date();
  public today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  public filterDate = this.today;
  public existData: boolean;
  public appointments: any;

  constructor(private appointmentService: AppointmentService, db: AngularFirestore) {
    this.getAppointments();
   }
  
  ngOnInit() {
  }
  
  getAppointments(){
    this.appointmentService.getAppointments(this.filterDate).subscribe(appointments=>{
      this.appointments = appointments;
      appointments.length > 0 ? this.existData = true : this.existData = false;
    })
  }
  
  deleteAppointment(id:string){
    const confirmacion = confirm('¿Seguro que quieres eliminar la cita?');
    if (confirmacion) {
      this.appointmentService.deleteAppointment(id);
    }
  }
}
