import { Component, OnInit } from '@angular/core';
import { AppointmentService } from '../../../services/appointment.service';
import { AppointmentInterface } from '../../../models/appointment';
import { AuthService } from '../../../services/auth.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../../shared/modal/modal.component';

@Component({
  selector: 'app-new-appointment',
  templateUrl: './new-appointment.component.html',
  styleUrls: ['./new-appointment.component.scss']
})
export class NewAppointmentComponent implements OnInit {
  date = new Date();
  today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
  maxDate = new Date(this.date.getFullYear() + 1, this.date.getMonth(), this.date.getDate());
  filterDate = this.today;
  appointments: any;
  existData: boolean;
  appointmentForm: FormGroup;
  newAppointment: AppointmentInterface = {
    uid: '',
    date: this.today,
    hour: '',
    estimatedDuration: 0,
    clientId: '',
    note: '',
  }
  constructor(private appointmentService: AppointmentService, private Auth: AuthService, public dialog: MatDialog) {
    this.Auth.getAuth().subscribe( auth => {this.newAppointment.uid = auth.uid;});
    this.appointmentForm = new FormGroup({
        'uid': new FormControl ('' , Validators.required),
        'date': new FormControl ('' , Validators.required),  
        'hour': new FormControl ('' , Validators.required),
        'estimatedDuration': new FormControl (2 , [Validators.required, Validators.min(1)]), 
        'clientId': new FormControl ('' , [Validators.required, Validators.pattern("([1-9]{1,})*-([0-9]{3,4})*-([0-9]{4,})")]),
        'note': new FormControl (''),
    })
    this.appointmentForm.setValue(this.newAppointment);
    this.getAppointments();
  }

  ngOnInit() {
  }

  getAppointments(){
    this.appointmentService.getAppointments(this.filterDate).subscribe(appointments=>{
      this.appointments = appointments;
      appointments.length > 0 ? this.existData = true : this.existData = false;
    })
  }

  resetForm(){
    this.newAppointment=({
      uid: this.newAppointment.uid,
      date: this.today,
      hour: '',
      estimatedDuration: 0,
      clientId: '',
      note: '',
    })
    this.appointmentForm.reset(this.newAppointment)
  }

  addAppointment(){
    this.appointmentForm.patchValue({uid: this.newAppointment.uid})
    if (this.appointmentForm.valid) {
      try {
        this.appointmentService.addAppointment(this.appointmentForm.value);
        this.dialog.open(ModalComponent, {data: { title: `Cita creada con éxito`, button2Text: 'aceptar'}});
        this.resetForm();
      } catch (error) {
        this.dialog.open(ModalComponent, {data: { title: 'Ocurrio un error', button2Text: 'aceptar'}});
      }
    }else{
      this.dialog.open(ModalComponent, {data: { title: 'Debe rellenar todos los campos', button2Text: 'aceptar'}});
    }
  }
}