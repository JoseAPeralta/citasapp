import { NgModule } from '@angular/core';
import { MatTabsModule, MatDialogModule, MatCardModule, MatButtonModule, MatSidenavModule, MatDividerModule, MatGridListModule, MatListModule, MatNativeDateModule,
        MatCheckboxModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatRadioModule, MatFormFieldModule, MatToolbarModule } from '@angular/material';


@NgModule({
    imports:[ MatToolbarModule, MatDialogModule, MatTabsModule,
        MatCardModule, MatButtonModule, MatSidenavModule, MatGridListModule, MatListModule, MatNativeDateModule,
        MatDividerModule, MatCheckboxModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatRadioModule, MatFormFieldModule
    ],
    exports:[ MatToolbarModule, MatDialogModule, MatTabsModule,
        MatCardModule, MatButtonModule, MatSidenavModule, MatGridListModule, MatListModule, MatNativeDateModule,
        MatDividerModule, MatCheckboxModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatRadioModule, MatFormFieldModule
    ]
})

export class MaterialModule{}