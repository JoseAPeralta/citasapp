export interface InvoiceInterface{
    id?: string;
    uid?: string;
    clientName?: string;
    clientId?: string;
    date?: any;
    amountOther?: number;
    amountTotal?: number;
    amountTax?: any;
    amountGross?: any;
    payments?: any;
    services?: any;
}