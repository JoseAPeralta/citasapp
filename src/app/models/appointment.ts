export interface AppointmentInterface {
    id?: any;
    uid?: any;
    date?: any;
    hour?: any;
    estimatedDuration?: number;
    clientId?: string;
    clientName?: string;
    note?: string;
  }

  export interface AppointmentIdInterface{
    id?: string;
  }