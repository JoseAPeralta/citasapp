export interface ServiceInterface {
    id?: string;
    uid?: string;
    service: string;
    price: number;
    profit?: number;
    tax?: number;
}